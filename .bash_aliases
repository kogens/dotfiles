# Use Python 3.8 as default
alias python='python3.8'
alias pip='python -m pip'

# Hide /loop* from df output (Snap makes a ton of these..)
alias dh='df -h | grep -v .*loop'

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias jekyll='bundle exec jekyll'
